# Scalar Movies

    Scalar movies is a simple application that allows you to search, comment and rate a list of movies. As an administrator you can create, update, and delete movies and comments.

## Live Demo

    You can see a demo of the application at: https://challenge.itvirtuous.com/

    To access the administrator you must log in with the administrator user:
        - Username: Admin
        - Password: Scalar-1

## Technologies

    - Frontend: Nextjs 13 with App Router and Typescript
    - Styling : Tailwind and Flowbite
    - Database : PostgresSQL
    - Backend : Python with Django
    - Deployment : Docker in Ubunutu 22 VPS, with Ngnix and SSL
    - External API: Moviesminidatabase and Imdb 
    - Auth : Custom Auth solution in Frontend, with Djoser in Backend

## List of pages

    -/: Homepage with the list of movies. You can filter by release_date,average_rating or title
    -/movies/movie_id: View with details of the movies and the form to add comments
    -/login : Login user
    -/register : Register user
    -/admin : Administration panel to create, update and delete movies, ratings and users

## Responsive site note

    All pages are responsive, but /admin pages are not responsive

## Special feature

    In the movie creation form, you can search for a movie in Moviesminidatabase, and fill out the form with the API information

## How run proyect locally

    Download repo and open terminal in project folder

    Create .env file at the root of the project with .env.example data

    Create .env.docker file at /front folder with .env.docker.example data
    
    Run:
    
        docker compose up -d 
    
    After the run containers have been generated run:

        docker compose run back python manage.py migrate

    (If the prev step fail, please restart backend container and try again)

    And it's done! Project running in http://localhost:3000

    The migrations generate the admin user to access in /admin pages 
        - Username: Admin
        - Password: Scalar-1

## Tests

    You can run test with docker compose run back python manage.py test