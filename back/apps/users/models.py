from django.db import models
from django.contrib.auth.models import User

def profile_photo_directory(instance, filename):
    return "profile/{0}/{1}".format(instance.user, filename)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    photo = models.ImageField(
        upload_to=profile_photo_directory, max_length=500, blank=True, null=True, default='profile/default/default.webp'
    )
    



