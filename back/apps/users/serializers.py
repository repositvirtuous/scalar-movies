from djoser.serializers import UserCreateSerializer
from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Profile

User = get_user_model()

class UserCreateSerializer(UserCreateSerializer):
    password = serializers.CharField(style={"input_type": "password"}, write_only=True)
    permissions = serializers.SerializerMethodField()
    group = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()

    class Meta(UserCreateSerializer.Meta):
        model = User
        fields = (
            'id',
            'email',
            'username',
            'first_name',
            'last_name',
            'get_full_name',
            'get_short_name',
            'permissions',
            'group',
            'photo',
            'password'
        )

    def get_permissions(self,instance):
        groups = instance.groups.all()
        
        group_permissions = []
        for group in groups:
            group_permissions.extend(list(group.permissions.all()))

        permission_list = []
        for permission in group_permissions:
            permission_dict = {
                'name': permission.name,
                'content_type': str(permission.content_type),
                'codename' : permission.codename
            }
            permission_list.append(permission_dict)
        
        return permission_list
    
    def get_group(self,instance):
        groups = instance.groups.all()

        if len(groups) > 0:
            return groups[0].name
        return ""
        
    def get_photo(self, user):
        
        user_profile = Profile.objects.get(user=user)
        return user_profile.photo.url