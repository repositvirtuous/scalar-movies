from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from utils.general import verifyParams
from rest_framework.exceptions import ValidationError
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from .serializers import UserCreateSerializer

def is_admin(user):
    return user.groups.filter(name='Admin').exists()

#This view is only used in the user admin panel
class UserView(APIView):

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

        type_request = self.request.method

        if type_request == 'GET':
            pass

        if type_request == 'POST':
            post_conditions = {
                'username' : {'is_string':True,'max_length':255,'required' : True},
                'group' : {'is_string' : True,'in_choices' : ['User','Admin'],'required' : True},
                'password' : {'is_string' : True,'required' : True} 
            }

            verify_params = verifyParams(self.request,post_conditions,type_request)

            if verify_params == True:
                return
            raise ValidationError(verify_params)

        if type_request == 'PUT':

            put_conditions = {
                'username' : {'is_string':True,'max_length':255,'required' : True},
                'group' : {'is_string' : True,'in_choices' : ['User','Admin'],'required' : True},
                'password' : {'is_string' : True} 
            }

            verify_params = verifyParams(self.request,put_conditions,type_request)

            if verify_params == True:
                return
            raise ValidationError(verify_params)


    @method_decorator(login_required, name='put')
    @method_decorator(user_passes_test(is_admin, login_url=None), name='put')
    def put(self,request,user_id,format = None):
        data = self.request.data

        try:
            user_id=int(user_id)
        except:
            return Response(
                {'error': 'ID must be an integer'},
                status=status.HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(id=user_id)
            user.username = data['username']
            group = Group.objects.get(name = data['group'])
            user.groups.clear()
            user.groups.add(group)

            if data['password']:
                user.set_password(data['password'])
            user.save()

            user = UserCreateSerializer(user)

            return Response({'user':user.data},status=status.HTTP_200_OK)

        except Exception as e:
            return Response(
                {'error': 'Fail updating the user'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        

