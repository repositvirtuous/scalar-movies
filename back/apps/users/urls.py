from django.urls import path

from .views import UserView

urlpatterns = [
    path('<user_id>', UserView.as_view(),name='user_view'),
]