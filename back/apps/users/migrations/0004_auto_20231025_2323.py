# Generated by Django 4.1 on 2023-10-25 23:23

from django.db import migrations
from django.contrib.auth.models import Group, Permission
from apps.users.models import Profile

groups_data = [
    {
        "name": "Admin",
        "permissions": []
    },
    {
        "name": "User",
        "permissions": []
    }
]

def add_groups_and_permissions(apps, schema_editor):
    for group_info in groups_data:
        group, _ = Group.objects.get_or_create(name=group_info["name"])
        permissions = []

        for codename in group_info.get("permissions", []):
            app_label, codename = codename.split(".")
            permission = Permission.objects.get(content_type__app_label=app_label, codename=codename)
            permissions.append(permission)

        group.permissions.set(permissions)

def create_admin_user(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    Group = apps.get_model('auth', 'Group')

    admin_group, created = Group.objects.get_or_create(name='Admin')

    if not User.objects.filter(username='Admin').exists():
        user = User.objects.create_user('Admin', password='Scalar-1')
        Profile.objects.create(user_id=user.id)
        user.groups.add(admin_group)
        user.is_staff = True
        user.save()

def remove_groups_and_permissions(apps, schema_editor):
    for group_info in groups_data:
        Group.objects.filter(name=group_info["name"]).delete()

class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_profile_user'),
    ]

    operations = [
        migrations.RunPython(add_groups_and_permissions, remove_groups_and_permissions),
        migrations.RunPython(create_admin_user)
    ]
