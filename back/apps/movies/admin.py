from django.contrib import admin

from .models import Movie,Rating
from django import forms

class MovieAdmin(admin.ModelAdmin):
    list_display = ('id','title','release_date','genre','plot')
    list_display_links = ()
    search_fields = ('title','release_date','genre')
    list_per_page = 25

admin.site.register(Movie,MovieAdmin)


class RatingAdminForm(forms.ModelForm):
    rating = forms.IntegerField(
        min_value=1,  
        max_value=10,  
    )

    class Meta:
        model = Rating
        fields = ('user','rating','comment')


class RatingAdmin(admin.ModelAdmin):
    list_display = ('id','user','rating','comment')
    list_display_links = ()
    search_fields = ('user','rating','comments')
    list_per_page = 25

admin.site.register(Rating,RatingAdmin)