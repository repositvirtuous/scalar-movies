from django.urls import path

from .views import MoviesView,MovieView,RatingsView,RatingView,UpdateMoviePhotoView

urlpatterns = [
    path('<movie_id>', MovieView.as_view(),name='movie_view'),
    path('', MoviesView.as_view(),name = 'movies_view'),
    path('photo/',UpdateMoviePhotoView.as_view(),name = 'movie_photo'),
    path('ratings/<rating_id>', RatingView.as_view(),name = 'rating_view'),
    path('ratings/', RatingsView.as_view(),name = 'ratings_view'),
]