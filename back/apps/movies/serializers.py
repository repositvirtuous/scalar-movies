from rest_framework import serializers
from .models import Movie,Rating

class RatingSerializer(serializers.ModelSerializer):

    username = serializers.SerializerMethodField()

    class Meta:
        model = Rating
        fields = [
            'id',
            'user',
            'rating',
            'comment',
            'movie',
            'username',
            'created_at'
        ]

    def get_username(self,instance):
        return instance.user.username
    


class MovieSerializer(serializers.ModelSerializer):

    ratings = serializers.SerializerMethodField()

    class Meta:
        model = Movie
        fields = [
            'id',
            'title',
            'release_date',
            'genre',
            'plot',
            'ratings',
            'average_rating',
            'photo'
        ]  

    def get_ratings(self,instance):

        ratings = instance.ratings.order_by('-created_at').all()
        ratings = RatingSerializer(ratings,many = True)

        return ratings.data