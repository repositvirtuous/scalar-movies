from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from apps.movies.models import Movie
from django.contrib.admin.models import User


class MoviesViewTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create(email='test@test.com', role='critic')
        self.movie = Movie.objects.create(title='Test movie',release_date = '1972-09-20',genre = 'Adventure,Drama,Sci-Fi,Action',plot = 'Test plot')

    def test_list_movies(self):
        
        url = reverse('movies_view')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['movies']), 1)
        self.assertEqual(response.data['movies'][0]['title'], 'Test movie')
