from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from .models import Movie,Rating
from .serializers import MovieSerializer,RatingSerializer
from utils.general import verifyParams
from rest_framework.exceptions import ValidationError
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import parser_classes
from django.db.models import Q

def is_admin(user):
    return user.groups.filter(name='Admin').exists()

#View to get a list of movies or create a new movie
class MoviesView(APIView):

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

        type_request = self.request.method

        if type_request == 'GET':

            get_conditions = {
                'sort_by': {'is_string':True,'in_choices':['rating','date']},
                'title' : {'is_string' : True},
                'order' : {'is_string':True,'in_choices' : ['asc','desc']},
                'limit' : {'is_number','is_positive'},
            }

            verify_params = verifyParams(self.request,get_conditions,type_request)

            if verify_params == True:
                return
            raise ValidationError(verify_params)

        if type_request == 'POST':

            post_conditions = {
                'title' : {'is_string':True,'max_length':255,'required':True},
                'release_date' : {'required' : True,'is_date' : True},
                'genre' : {'required' : True,'max_length' : 100},
                'plot' : {'required' : True,'max_length' : 4000}
            }

            verify_params = verifyParams(self.request,post_conditions,type_request)
            if verify_params == True:
                return
            raise ValidationError(verify_params)

    def get(self, request, format=None):

        sort_by = request.query_params.get('sort_by')
        order = request.query_params.get('order')
        limit = request.query_params.get('limit')
        title = request.query_params.get('title')

        if not order:
            'asc'
        
        if not limit:
            limit = 25

        order_by = 'id'

        if sort_by == 'rating':
            #Order by average_rating
            order_by = 'average_rating'

        if sort_by == 'date':
            #Order by release_date
            order_by = 'release_date'
        
        if order == 'desc':
            order_by = '-' + order_by

        if title:
            movies = Movie.objects.order_by(order_by).filter(
                Q(title__icontains=title) 
            )[:int(limit)]
        else:
            movies = Movie.objects.order_by(order_by).all()[:int(limit)]

        movies = MovieSerializer(movies,many = True)

        return Response({'movies':movies.data},status=status.HTTP_200_OK)

    @method_decorator(user_passes_test(is_admin, login_url=None), name='post')
    @method_decorator(login_required, name='post')
    def post(self,request,format = None):

        data = self.request.data

        try:
            movie = Movie.objects.create(**data)
            movie = MovieSerializer(movie)

            return Response({'movie':movie.data},status=status.HTTP_200_OK)

        except Exception as e:
            return Response(
                {'error': 'Fail creating the movie'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
    
#View to get,update or delete a specific movie
class MovieView(APIView):

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

        type_request = self.request.method

        if type_request == 'GET':
            pass

        if type_request == 'POST':
            pass
        
        if type_request == 'PUT':

            put_conditions = {
                'title' : {'is_string':True,'max_length':255},
                'release_date' : {'is_date' : True},
                'genre' : {'max_length' : 100},
                'plot' : {'max_length' : 4000}
            }

            verify_params = verifyParams(self.request,put_conditions,type_request)

            if verify_params == True:
                return
            raise ValidationError(verify_params)

    def get(self, request, movie_id, format=None):

        try:
            movie_id=int(movie_id)
        except:
            return Response(
                {'error': 'ID must be an integer'},
                status=status.HTTP_400_BAD_REQUEST)

        if Movie.objects.filter(id=movie_id).exists():
            movie = Movie.objects.get(id=movie_id)
            movie = MovieSerializer(movie)

            return Response({'movie': movie.data}, status=status.HTTP_200_OK)

        return Response(
                    {'error': 'Movie does not exist'},
                    status=status.HTTP_404_NOT_FOUND)

    @method_decorator(login_required, name='put')
    @method_decorator(user_passes_test(is_admin, login_url=None), name='post')
    def put(self,request,movie_id,format = None):
        data = self.request.data

        try:
            movie_id=int(movie_id)
        except:
            return Response(
                {'error': 'ID must be an integer'},
                status=status.HTTP_400_BAD_REQUEST)

        try:
            movie = Movie.objects.get(id=movie_id)
            for field in data:
                setattr(movie,field,data[field])
            movie.save()
            movie = MovieSerializer(movie)

            return Response({'movie':movie.data},status=status.HTTP_200_OK)

        except Exception as e:
            return Response(
                {'error': 'Fail updating the movie'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

    @method_decorator(login_required, name='delete')
    @method_decorator(user_passes_test(is_admin, login_url=None), name='delete')
    def delete(self,request,movie_id,format = None):
        try:
            movie_id=int(movie_id)
        except:
            return Response(
                {'error': 'ID must be an integer'},
                status=status.HTTP_400_BAD_REQUEST)

        try:
            
            movie = Movie.objects.get(id=movie_id)
            movie.delete()
            return Response({'status':'Success'},status=status.HTTP_200_OK)

        except Exception as e:
            return Response(
                {'error': 'Fail deleting the movie'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

#View to get a list of ratings and create a new rating
class RatingsView(APIView):
    permission_classes = (permissions.AllowAny, )

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

        type_request = self.request.method

        if type_request == 'GET':
            pass

        if type_request == 'POST':
            post_conditions = {
                'movie': {'required': True},
                'user' : {'required' : True},
                'rating': {'is_string': True, 'in_choices': ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'], 'required': True},
                'comment': {'max_length': 4000}
            }

            verify_params = verifyParams(self.request, post_conditions, type_request)

            if verify_params == True:
                return
            raise ValidationError(verify_params)

    def get(self, request, format=None):
        ratings = Rating.objects.order_by('-created_at').all()
        ratings = RatingSerializer(ratings, many=True)
        return Response({'ratings': ratings.data}, status=status.HTTP_200_OK)

    @method_decorator(login_required, name='post')
    def post(self, request, format=None):
        data = self.request.data

        try:
            movie = Movie.objects.get(id = data['movie'])
            user = User.objects.get(id = data['user'])
            comment = data['comment']
            rating_value = data['rating']

            rating = Rating.objects.create(movie = movie,user=user,comment=comment,rating=rating_value) 
            rating = RatingSerializer(rating)

            return Response({'rating': rating.data}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(
                {'error': 'Fail creating the rating'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

#View to get,update or delete a specific rating
class RatingView(APIView):
    permission_classes = (permissions.AllowAny, )

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

        type_request = self.request.method

        if type_request == 'GET':
            pass

        if type_request == 'POST':
            pass

        if type_request == 'PUT':
            put_conditions = {
                'movie': {'required': True},
                'user' : {'required' : True},
                'rating': {'is_string': True, 'in_choices': ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']},
                'comment': {'max_length': 4000}
            }

            verify_params = verifyParams(self.request, put_conditions, type_request)

            if verify_params == True:
                return
            raise ValidationError(verify_params)

        if type_request == 'DELETE':
            pass
        
    def get(self, request, rating_id, format=None):
        try:
            rating_id = int(rating_id)
        except:
            return Response(
                {'error': 'ID must be an integer'},
                status=status.HTTP_400_BAD_REQUEST)

        if Rating.objects.filter(id=rating_id).exists():
            rating = Rating.objects.get(id=rating_id)
            rating = RatingSerializer(rating)

            return Response({'rating': rating.data}, status=status.HTTP_200_OK)

        return Response(
            {'error': 'Rating does not exist'},
            status=status.HTTP_404_NOT_FOUND
        )

    @method_decorator(login_required, name='put')
    @method_decorator(user_passes_test(is_admin, login_url=None), name='put')
    def put(self, request, rating_id, format=None):
        data = self.request.data

        try:
            rating_id = int(rating_id)
        except:
            return Response(
                {'error': 'ID must be an integer'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            rating = Rating.objects.get(id=rating_id)
            movie = Movie.objects.get(id = data['movie'])
            user = User.objects.get(id = data['user'])

            rating.movie = movie
            rating.user = user
            rating.comment = data['comment']
            rating.rating = data['rating']
            rating.save()
            rating = RatingSerializer(rating)

            return Response({'rating': rating.data}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(
                {'error': 'Fail updating the rating'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

    @method_decorator(login_required, name='delete')
    @method_decorator(user_passes_test(is_admin, login_url=None), name='delete')
    def delete(self, request, rating_id, format=None):
        try:
            rating_id = int(rating_id)
        except:
            return Response(
                {'error': 'ID must be an integer'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            rating = Rating.objects.get(id=rating_id)
            rating.delete()
            return Response({'status': 'Success'}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(
                {'error': 'Fail deleting the rating'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

@parser_classes([MultiPartParser])  
class UpdateMoviePhotoView(APIView):
    def put(self, request, format=None):
        try:
            data = self.request.data

            photo = data['photo']
            movie_id = data['movie_id']

            movie = Movie.objects.get(id = movie_id)
            movie.photo = photo
            movie.save()
            
            movie = MovieSerializer(movie)

            return Response(
                {'movie': movie.data},
                status=status.HTTP_200_OK
            )
        except Exception as e:

            return Response(
                {'error': 'Fail updating movie photo'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )