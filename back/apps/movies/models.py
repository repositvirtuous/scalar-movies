from django.db import models
from django.db import models
from django.conf import settings
from utils.general import recalculateMovieAverageRating
User = settings.AUTH_USER_MODEL

rating_choices = [(str(i), str(i)) for i in range(1, 11)]

def movies_photo_directory(instance, filename):
    return "movie/{0}/{1}".format(instance.id, filename)
    
class Movie(models.Model):
    title = models.CharField(max_length=255)
    release_date = models.DateField()
    genre = models.CharField(max_length=100)
    plot = models.TextField(max_length=4000)
    photo = models.ImageField(
        upload_to=movies_photo_directory, max_length=500, default='default/default.jpg'
    )
    average_rating = models.DecimalField(max_digits=4,decimal_places=2,default=0)
    created_at = models.DateTimeField(auto_now_add=True,null=True)
    updated_at = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.title

class Rating(models.Model):
    
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE,related_name='ratings')
    rating = models.CharField(
        max_length=2,
        choices=rating_choices,
        default='1'
    )
    comment = models.TextField(max_length=4000,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True)
    updated_at = models.DateTimeField(auto_now=True,null=True)

    def save(self, *args, **kwargs):
        super(Rating, self).save(*args, **kwargs)
        #Recalculate average_rating of movie
        recalculateMovieAverageRating(self.movie)




