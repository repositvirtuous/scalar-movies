from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .data.movie_database import searchMovieByTitle,getMovieInfoById

class SearchMoviesByTitleView(APIView):

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

    def get(self,request,movietitle,format = None):

        try: 
            json = searchMovieByTitle(movietitle)

            return Response({'movies':json},status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error" : True},status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetMovieInfoById(APIView):

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

    def get(self,request,movieid,format = None):

        try:
            json = getMovieInfoById(movieid)

            return Response({'movie':json},status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error" : True},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
