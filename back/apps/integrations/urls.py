from django.urls import path

from .views import SearchMoviesByTitleView,GetMovieInfoById

urlpatterns = [
    path('searchmovie/<movietitle>', SearchMoviesByTitleView.as_view()),
    path('getmovie/<movieid>', GetMovieInfoById.as_view()),
]