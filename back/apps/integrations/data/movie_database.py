import environ
env = environ.Env()
environ.Env.read_env()
import requests

#Search a list of movies in moviesminidatabase by title
def searchMovieByTitle(movie_title : str):

        url = "https://moviesminidatabase.p.rapidapi.com/movie/imdb_id/byTitle/" + movie_title + "/"

        X_RapidAPI_Key = env('X_RapidAPI_Key')
        X_RapidAPI_Host = env('X_RapidAPI_Host')

        headers = {
            "X-RapidAPI-Key": X_RapidAPI_Key,
            "X-RapidAPI-Host": X_RapidAPI_Host
        }
        try:
            response = requests.get(url, headers=headers)  
            json = response.json()

            return json
        except Exception as e:
           raise e


#Get extra info about a movie with imdbid
def getMovieInfoById(movie_id : str):
        url = "https://moviesminidatabase.p.rapidapi.com/movie/id/" + movie_id +"/"

        X_RapidAPI_Key = env('X_RapidAPI_Key')
        X_RapidAPI_Host = env('X_RapidAPI_Host')

        headers = {
            "X-RapidAPI-Key": X_RapidAPI_Key,
            "X-RapidAPI-Host": X_RapidAPI_Host
        }
        
        try:
            response = requests.get(url, headers=headers)  

            json = response.json()

            return json
        except Exception as e:
            raise e