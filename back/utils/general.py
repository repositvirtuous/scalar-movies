from datetime import datetime

#This function allows to centralize the parameter validation logic for REST methods.
#Can validate: required,is_string,is_number,max_lenght,min_lenght,is_positive,in_choices and is_date
def verifyParams(request , field_conditions : dict,type):
    errors = []

    for field, conditions in field_conditions.items():
        
        value = None
        
        if type == 'GET':
            value = request.query_params.get(field)
        
        if type == 'POST' or type == 'PUT':
            if field in request.data:
                value = request.data[field]

        if value is None:
            if 'required' in conditions:
                errors.append(f"The '{field}' field is required.")
            continue
                        
        if 'is_string' in conditions:

            if isinstance(value,str):
                pass
            else:
                errors.append(f"The '{field}' field should be a string.")

        if 'is_number' in conditions and not value.isdigit():
            errors.append(f"The '{field}' field should be an integer.")
        
        if 'max_length' in conditions and len(value) > conditions['max_length']:
            errors.append(f"The '{field}' field exceeds the maximum allowed length.")
        
        if 'min_length' in conditions and len(value) < conditions['min_length']:
            errors.append(f"The '{field}' field does not meet the minimum required length.")

        if 'is_positive' in conditions:
            try:
                numeric_value = float(value)
                if numeric_value <= 0:
                    errors.append(f"The '{field}' field should be a positive number.")
            except ValueError:
                errors.append(f"The '{field}' field should be a numeric value.")

        if 'in_choices' in conditions:
            choices = conditions['in_choices']
            if value not in choices:
                errors.append(f"The '{field}' field should be one of the allowed choices: {', '.join(choices)}")

        if 'is_date' in conditions:
            try:
                datetime.strptime(value, '%Y-%m-%d')
            except ValueError:
                errors.append(f"The '{field}' field should be a date in the format YYYY-MM-DD.")

    if not errors:
        return True
    else:
        return {'error' : errors}


# This function calculates the average rating every time the rating of a movie is updated. 
# This makes it unnecessary to recalculate the average every time you want to obtain it.
def recalculateMovieAverageRating(movie):
    
    ratings = movie.ratings

    count = ratings.count()
    total = 0
    for rating in ratings.all():
        total+=int(rating.rating)
    
    new_average_rating = total / count

    movie.average_rating = new_average_rating
    movie.save()



