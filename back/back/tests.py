from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from apps.movies.models import Movie,Rating
from django.contrib.auth.models import User,Group


class MoviesViewTest(APITestCase):
    def setUp(self):
        admin_group, _ = Group.objects.get_or_create(name='Admin')
        self.user = User.objects.create_user('admin_user', password='password')
        self.user.groups.add(admin_group)
        self.movie = Movie.objects.create(title='Test movie',release_date = '1972-09-20',genre = 'Adventure,Drama,Sci-Fi,Action',plot = 'Test plot')

    def test_list_movies(self):
        
        url = reverse('movies_view')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['movies']), 1)
        self.assertEqual(response.data['movies'][0]['title'], 'Test movie')

    def test_create_movie(self):
        url = reverse('movies_view') 
        data = {
            'title': 'New Movie',
            'release_date': '2023-10-26',
            'genre': 'Action',
            'plot': 'A new movie plot.'
        }

        self.client.force_authenticate(self.user)  

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Movie.objects.count(), 2) 

    def test_create_guest_movie(self):
        url = reverse('movies_view') 
        data = {
            'title': 'New Movie',
            'release_date': '2023-10-26',
            'genre': 'Action',
            'plot': 'A new movie plot.'
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class MovieViewTest(APITestCase):
    def setUp(self):
        admin_group, _ = Group.objects.get_or_create(name='Admin')
        self.user = User.objects.create_user('admin_user', password='password')
        self.user.groups.add(admin_group)
        self.movie = Movie.objects.create(title='Test movie', release_date='1972-09-20', genre='Adventure,Drama,Sci-Fi,Action', plot='Test plot')

    def test_get_movie(self):
        movie_id = self.movie.id
        url = reverse('movie_view', kwargs={'movie_id': movie_id})
        
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['movie']['title'], 'Test movie')

    def test_put_movie(self):

        self.client.force_authenticate(user=self.user)

        movie_id = self.movie.id
        url = reverse('movie_view', kwargs={'movie_id': movie_id})
        updated_data = {
            'title': 'Updated Movie',
            'release_date': '1999-12-31',
            'genre': 'Comedy',
            'plot': 'Updated plot',
        }
        response = self.client.put(url, updated_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.movie.refresh_from_db() 
        self.assertEqual(self.movie.title, updated_data['title'])
        self.assertEqual(str(self.movie.release_date), updated_data['release_date'])
        self.assertEqual(self.movie.genre, updated_data['genre'])
        self.assertEqual(self.movie.plot, updated_data['plot'])
    
    def test_put_guest_user(self):
        movie_id = self.movie.id
        url = reverse('movie_view', kwargs={'movie_id': movie_id})

        updated_data = {
            'title': 'Updated Movie',
            'release_date': '1999-12-31',
            'genre': 'Comedy',
            'plot': 'Updated plot',
        }

        response = self.client.put(url, updated_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_movie(self):

        self.client.force_authenticate(user=self.user)

        movie_id = self.movie.id
        url = reverse('movie_view', kwargs={'movie_id': movie_id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(Movie.objects.filter(id=movie_id).exists())

    def test_delete_guest_movie(self):

        movie_id = self.movie.id
        url = reverse('movie_view', kwargs={'movie_id': movie_id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class RatingsViewTest(APITestCase):
    def setUp(self):
        admin_group, _ = Group.objects.get_or_create(name='Admin')
        self.user = User.objects.create_user('admin_user', password='password')
        self.user.groups.add(admin_group)
        self.movie = Movie.objects.create(title='Test movie', release_date='1972-09-20', genre='Adventure,Drama,Sci-Fi,Action', plot='Test plot')
        self.rating = Rating.objects.create(movie=self.movie, user=self.user, rating=8, comment='Great movie')

    def test_list_ratings(self):
        url = reverse('ratings_view')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['ratings']), 1)
        self.assertEqual(response.data['ratings'][0]['rating'], '8')

    def test_create_rating(self):
        url = reverse('ratings_view')
        data = {
            'movie': self.movie.id,
            'user': self.user.id,
            'rating': "9",
            'comment': 'Awesome movie'
        }

        self.client.force_authenticate(self.user)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Rating.objects.count(), 2)

    def test_create_guest_rating(self):
        url = reverse('ratings_view')
        data = {
            'movie': self.movie.id,
            'user': self.user.id,
            'rating': "9",
            'comment': 'Awesome movie'
        }

        response = self.client.post(url, data, format='json')

        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

class RatingViewTest(APITestCase):
    def setUp(self):
        admin_group, _ = Group.objects.get_or_create(name='Admin')
        self.user = User.objects.create_user('admin_user', password='password')
        self.user.groups.add(admin_group)
        self.movie = Movie.objects.create(title='Test movie', release_date='1972-09-20', genre='Adventure,Drama,Sci-Fi,Action', plot='Test plot')
        self.rating = Rating.objects.create(movie=self.movie, user=self.user, rating=8, comment='Great movie')

    def test_get_rating(self):
        rating_id = self.rating.id
        url = reverse('rating_view', kwargs={'rating_id': rating_id})
        
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rating']['rating'], "8")

    def test_put_rating(self):
        self.client.force_authenticate(user=self.user)

        rating_id = self.rating.id
        url = reverse('rating_view', kwargs={'rating_id': rating_id})
        updated_data = {
            'movie': self.movie.id,
            'user': self.user.id,
            'rating': "9",
            'comment': 'Awesome movie'
        }

        response = self.client.put(url, updated_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.rating.refresh_from_db() 
        self.assertEqual(self.rating.rating, "9")
        self.assertEqual(self.rating.comment, 'Awesome movie')

    def test_put_guest_rating(self):
        rating_id = self.rating.id
        url = reverse('rating_view', kwargs={'rating_id': rating_id})

        updated_data = {
            'movie': self.movie.id,
            'user': self.user.id,
            'rating': "9",
            'comment': 'Awesome movie'
        }

        response = self.client.put(url, updated_data, format='json')

        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_rating(self):
        self.client.force_authenticate(user=self.user)

        rating_id = self.rating.id
        url = reverse('rating_view', kwargs={'rating_id': rating_id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(Rating.objects.filter(id=rating_id).exists())

    def test_delete_guest_rating(self):
        rating_id = self.rating.id
        url = reverse('rating_view', kwargs={'rating_id': rating_id})
        response = self.client.delete(url)

        self.assertNotEqual(response.status_code, status.HTTP_200_OK)
        

class UserViewTest(APITestCase):
    def setUp(self):
        admin_group, _ = Group.objects.get_or_create(name='Admin')
        self.user = User.objects.create_user('admin_user', password='password')
        self.user.groups.add(admin_group)


    def test_put_user(self):
        user_id = self.user.id
        url = reverse('user_view', kwargs={'user_id': user_id})
        updated_data = {
            'username': 'updated_user',
            'group': 'Admin',
            'password': 'new_password',
        }

        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, updated_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.user.refresh_from_db() 
        self.assertEqual(self.user.username, updated_data['username'])
        self.assertTrue(self.user.groups.filter(name='Admin').exists())
        self.assertTrue(self.user.check_password(updated_data['password']))

    def test_put_guest_user(self):
        user_id = self.user.id
        url = reverse('user_view', kwargs={'user_id': user_id})
        updated_data = {
            'username': 'updated_user',
            'group': 'Admin',
            'password': 'new_password',
        }

        response = self.client.put(url, updated_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)