
const base_url = process.env.NEXT_PUBLIC_APP_API_URL
const base_url_server = process.env.NEXT_PUBLIC_APP_API_SERVER_URL

export const generateUrl = (endpoint : string) => {

    const base_url_to_user = typeof window == 'undefined' ? base_url_server  : base_url
    const url = base_url_to_user + "/api/" + endpoint

    return url
}


export const getHeaders = (token : string) => {
    let headers : Record<string,string> = {'Content-Type': 'application/json'}

    if(token){
        headers['Authorization'] = `JWT ${token}`
    }

    return headers
}