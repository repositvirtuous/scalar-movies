
const base_url = process.env.NEXT_PUBLIC_APP_API_URL
const base_url_server = process.env.NEXT_PUBLIC_APP_API_SERVER_URL

export const getImage = (path : string) => {
    return  base_url  + path
}

export const getImageServer = (path : string) => {
    return base_url_server + path
}