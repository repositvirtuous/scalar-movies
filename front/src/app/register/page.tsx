'use client'
import { useSession } from "@/contexts/AuthContext"
import {useRouter} from 'next/navigation'
import { useEffect } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const Register = () => {

    const {user,register,loading} = useSession()
    const router = useRouter()

    useEffect(() => {
        if(!loading && user){
            router.push("/")
        }
    },[loading])

    const submitForm = async (e : any) => {
        e.preventDefault()

        const username = document.getElementById('username') as HTMLInputElement
        const password = document.getElementById('password') as HTMLInputElement
        const confirm_password = document.getElementById('confirm-password') as HTMLInputElement
        
        if(password.value != confirm_password.value){
            toast.error('The passwords are not the same')
            return
        }

        if(register){
            const response = await register(username.value,password.value)

            if(response == true){
                toast('You have been successfully registered. ¡Now you can login!. Redirect...')
                setTimeout(() => {
                    router.push('/login')
                },3000)
            }else{
                toast.error(response)
            }
        }
    }

    return (
        <main className="flex min-h-screen flex-col items-center p-24 container mx-auto p-4">
            <form onSubmit={submitForm} className = "w-full sm:w-1/3">

                <h1 className="my-4 block mb-2 text-xl font-medium text-gray-900 dark:text-white text-center">Register</h1>

                <div className="mb-6">
                    <label htmlFor="username" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
                    <input type="username" id="username" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="" required/>
                </div>
                <div className="mb-6">
                    <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                    <input type="password" id="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required/>
                </div>
                <div className="mb-6">
                    <label htmlFor="confirm-password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Confirm password</label>
                    <input type="password" id="confirm-password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required/>
                </div>
                <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                    Already have an account? <a href="/login" className="font-medium text-primary-600 hover:underline dark:text-primary-500">Login here</a>
                </p>
                <button type="submit" className="mt-4 w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Register</button>
            </form>
            <ToastContainer position="top-center" />
        </main>
    )
}

export default Register