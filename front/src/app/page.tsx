import { getMovies } from '@/api/movie/list'
import MovieCard from '@/components/server/MovieCard'
import MovieSearch from '@/components/client/MovieSearch'

type HomeProps = {
  params: {slug : string};
  searchParams: { [key: string]: string | string[] | undefined };
};

export default async function Home(props : HomeProps) {

  const order_by = props.searchParams.order_by as string
  const title = props.searchParams.title as string
  const movies = await getMovies(order_by,title)
  return (
    <main className="flex min-h-screen flex-col items-center p-24 container mx-auto p-4">
      {/*Filters*/}
      <MovieSearch />

      {/*Movies*/ }
      <div className="grid grid-cols-1 md:grid-cols-5 gap-4 mt-8 flex-grow">
        {movies.map((movie) => {
          return (
            <MovieCard key = {movie.id} movie={movie} />
          )
        })}
      </div>
  </main>
  )
}
