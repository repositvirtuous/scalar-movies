'use client'
import { useSession } from "@/contexts/AuthContext"
import {useRouter} from 'next/navigation'
import { useEffect } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login = () => {

    const {user,login,loading} = useSession()
    const router = useRouter()

    useEffect(() => {
        if(!loading && user){
            router.push("/")
        }
    },[loading])


    const submitForm = async (e : any) => {
        e.preventDefault()

        const username = document.getElementById('username') as HTMLInputElement
        const password = document.getElementById('password') as HTMLInputElement

        if(login){
            const response = await login(username.value,password.value)
            if(response){
                router.push('/')
            }else{
                toast.error('Invalid credentials')
            }
        }
    }

    return (
        <main className="flex min-h-screen flex-col items-center p-24 container mx-auto p-4">
            <form onSubmit={submitForm} className = "w-full sm:w-1/3">

                <h1 className="my-4 block mb-2 text-xl font-medium text-gray-900 dark:text-white text-center">Login</h1>

                <div className="mb-6">
                    <label htmlFor="username" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
                    <input type="username" id="username" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="" required/>
                </div>
                <div className="mb-6">
                    <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                    <input type="password" id="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required/>
                </div>
                <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                      Don’t have an account yet? <a href="/register" className="font-medium text-primary-600 hover:underline dark:text-primary-500">Register</a>
                </p>
                <button type="submit" className="mt-4 w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Login</button>
            </form>
            <ToastContainer position="top-center" />
        </main>
    )
}

export default Login