import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import './globals.css'
import { AuthProvider } from '@/contexts/AuthContext'
import Header from '@/components/client/Header'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Scalar movies',
  description: 'Site when you can add ratings and see informations about movies',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <AuthProvider>
          <Header />
          <main className="">{children}</main>
          <footer className="text-white text-center p-4">
            <div className="container mx-auto">Scalar Movies</div>
          </footer>
          <script src="https://unpkg.com/flowbite@1.5.1/dist/flowbite.js" async></script>
        </AuthProvider>
      </body>
    </html>
  )
}
