import { getMovies } from "@/api/movie/list"
import ToastClient from "@/components/client/ToastClient";
import UsersTable from "@/components/client/UsersTable";
import SideBarAdmin from "@/components/server/SideBarAdmin"

type MoviesListProps = {
    params: {slug : string};
    searchParams: { [key: string]: string | string[] | undefined };
  };

  
const MoviesList = async (props:MoviesListProps) => {

    const movies = await getMovies('','')

    const post_save = props.searchParams.post_save as string
    const post_create = props.searchParams.post_create as string

    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
           <div className = "p-1 w-2/3">
                <a href="/admin/users/create" type="button" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Add user</a>
                <UsersTable movies = {movies} /> 
           </div>
           <ToastClient message={post_save == 'true' ? "User successfully updated" : "User successfully created"} show = {post_save == 'true' || post_create == 'true'} />
        </main>
    )
}

export default MoviesList