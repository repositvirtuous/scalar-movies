import UserForm from "@/components/client/forms/UsersForm"
import SideBarAdmin from "@/components/server/SideBarAdmin"

const MovieCreate = async ({ params }: { params: { slug: string } }) =>  { 



    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
           <UserForm user_id={"0"} type = "create"  />
        </main>
    )
}

export default MovieCreate