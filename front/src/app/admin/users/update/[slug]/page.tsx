import UserForm from "@/components/client/forms/UsersForm"

import SideBarAdmin from "@/components/server/SideBarAdmin"

const UserUpdate = async ({ params }: { params: { slug: string } }) =>  { 


    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
           <UserForm user_id = {params.slug} type = "update"  />
        </main>
    )
}

export default UserUpdate