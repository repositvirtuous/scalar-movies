'use client'

import SideBarAdmin from "@/components/server/SideBarAdmin"
import { useSession } from "@/contexts/AuthContext"
import { useRouter } from "next/navigation"
import { useEffect } from "react"
const Admin = () => {

    const {user,loading} = useSession()
    const router = useRouter()
    
    useEffect(() => {
        if(!loading && user){
            if(user.group !== 'Admin'){
                router.push('/')
            }
        }
    },[loading])


    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
        </main>
    )
}

export default Admin