import { getRatings } from "@/api/rating/list";
import DeleteButton from "@/components/client/DeleteButton";
import ToastClient from "@/components/client/ToastClient";
import SideBarAdmin from "@/components/server/SideBarAdmin"

type MoviesListProps = {
    params: {slug : string};
    searchParams: { [key: string]: string | string[] | undefined };
  };

  
const RatingsList = async (props:MoviesListProps) => {

    const ratings = await getRatings()

    const post_save = props.searchParams.post_save as string
    const post_create = props.searchParams.post_create as string

    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
           <div className = "p-1 w-2/3">
                <a href="/admin/ratings/create" type="button" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Add rating</a>
                
                <table className="w-full mt-4 text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                id
                            </th>
                            <th scope="col" className="px-6 py-3">
                                User
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Movie
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Rating value
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Comment
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Edit
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Delete
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        {ratings.map((rating) => {
                            return (
                            <tr key = {rating.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                <td scope="row" className="px-6 py-4">
                                    <a className = "underline text-blue-600 hover:text-blue-800 visited:text-purple-600" href = {"/admin/ratings/update/" + rating.id}>{rating.id}</a>
                                </td>
                                <td className="px-6 py-4">
                                    <a className = "underline text-blue-600 hover:text-blue-800 visited:text-purple-600" href = {"/admin/users/update/" + rating.user}>{rating.user}</a>
                                </td>
                                <td className="px-6 py-4">
                                    <a className = "underline text-blue-600 hover:text-blue-800 visited:text-purple-600" href = {"/admin/movies/update/" + rating.movie}>{rating.movie}</a>
                                </td>
                                <td className="px-6 py-4">
                                    {rating.rating}
                                </td>
                                <td className="px-6 py-4">
                                    {rating.comment}
                                </td>
                                <td scope="row" className="px-6 py-4">
                                    <a className = "focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800" href = {"/admin/ratings/update/" + rating.id}>Edit</a>
                                </td>
                                <td scope="row" className="px-6 py-4">
                                    <DeleteButton id = {rating.id} type = "rating"/>
                                </td>
                            </tr>
                            )
                        })}

                    </tbody>
                </table>
           </div>
           <ToastClient message={post_save == 'true' ? "Rating successfully updated" : "Rating successfully created"} show = {post_save == 'true' || post_create == 'true'} />
        </main>
    )
}

export default RatingsList