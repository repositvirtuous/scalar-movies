import { getMovies } from "@/api/movie/list"
import { getMovie } from "@/api/movie/movie"
import { getRating } from "@/api/rating/rating"
import { Movie, Rating } from "@/api/typescript/interfaces"
import RatingForm from "@/components/client/forms/RatingForm"
import SideBarAdmin from "@/components/server/SideBarAdmin"
import { redirect } from 'next/navigation'

const RatingUpdate = async ({ params }: { params: { slug: string } }) =>  { 

    const rating : Rating | false = await getRating(params.slug) 
    const movies : Movie[] = await getMovies('','') 
    if(!rating){
        redirect('/')
    }

    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
           <RatingForm movies = {movies} rating = {rating} type = "update"  />
        </main>
    )
}

export default RatingUpdate