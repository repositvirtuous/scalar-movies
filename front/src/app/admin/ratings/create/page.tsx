import { getMovies } from "@/api/movie/list"
import { Movie, Rating } from "@/api/typescript/interfaces"
import RatingForm from "@/components/client/forms/RatingForm"
import SideBarAdmin from "@/components/server/SideBarAdmin"

const RatingCreate = async ({ params }: { params: { slug: string } }) =>  { 
    
    const movies : Movie[] = await getMovies('','') 
    
    const rating = {
        id : 0,
        user : 1,
        rating : 1,
        comment : '',
        movie : 1
    } as Rating

    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
           <RatingForm rating={rating} type="create" movies={movies}  />
        </main>
    )
}

export default RatingCreate