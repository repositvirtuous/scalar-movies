import { getMovie } from "@/api/movie/movie"
import { Movie } from "@/api/typescript/interfaces"
import MovieForm from "@/components/client/forms/MovieForm"
import SideBarAdmin from "@/components/server/SideBarAdmin"
import { redirect } from 'next/navigation'

const MovieUpdate = async ({ params }: { params: { slug: string } }) =>  { 

    const movie : Movie | false = await getMovie(params.slug) 

    if(!movie){
        redirect('/')
    }

    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
           <MovieForm movie = {movie} type = "update"  />
        </main>
    )
}

export default MovieUpdate