import { Movie } from "@/api/typescript/interfaces"
import MovieForm from "@/components/client/forms/MovieForm"
import SideBarAdmin from "@/components/server/SideBarAdmin"

const MovieCreate = async ({ params }: { params: { slug: string } }) =>  { 

    const movie = {
         title : '',
         release_date : '',
         plot : '',
         genre : '',
         id : 0
    } as Movie

    return (
        <main className="flex min-h-screen flex-col items-center p-24 relative">
           <SideBarAdmin />
           <MovieForm movie = {movie} type = "create"  />
        </main>
    )
}

export default MovieCreate