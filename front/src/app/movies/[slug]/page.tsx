import { getMovie } from "@/api/movie/movie"
import { Movie } from "@/api/typescript/interfaces"
import { redirect } from 'next/navigation'
import Image from "next/image";
import { getImage, getImageServer } from "@/utils/image";
import { createRating } from "@/api/rating/rating";
import CommentsList from "@/components/client/CommentsLIst";


const ScoreCircle = ({ score, color } : {score:any,color:any}) => {
    return (
      <div style={{backgroundColor:color}} className={`mt-2 w-16 h-16 rounded-full flex items-center justify-center`}>
        <span className="text-white text-lg font-semibold">{score}</span>
      </div>
    );
  };


export default async function Movie({ params }: { params: { slug: string } }) { 

    const movie : Movie | false = await getMovie(params.slug) 

    if(!movie){
        redirect('/')
    }

    return (
        <main className="flex min-h-screen flex-col items-center p-24 container mx-auto p-4">
            <div className="flex flex-col sm:flex-row lg:flex mt-4 justify-center">

                {/* CONTENT */}
                <div className="">
                    <div className="relative">
                        <Image src={getImageServer(movie.photo)} width={500} height={700} alt = "" />
                    </div>
                </div>

                {/* SIDEBAR */}
                <div className="flex flex-col w-full lg:w-[45%] pt-10 lg:pt-0 lg:pl-7 xl:pl-9 2xl:pl-10">
                    <h5 style={{marginBottom:'0px'}} className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{movie.title}</h5>
                    <span className="mt-2 text-gray-400 text-sm">
                        <strong>Release date:</strong> {movie.release_date}
                    </span> 
                    <span className="mt-2 text-gray-400 text-sm">
                        <strong>Genres:</strong> {movie.genre}
                    </span> 
                    <ScoreCircle score = {movie.average_rating} color = {"#004A8B"} />
                    <div className="mt-2">                        
                        {movie.plot}
                    </div>
                </div>
            </div>
            <CommentsList movie={movie} />
        </main>
    )


}

