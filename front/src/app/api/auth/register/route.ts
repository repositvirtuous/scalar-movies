export const dynamic = 'force-dynamic'
import { NextRequest, NextResponse } from "next/server";
import { generateUrl } from "@/utils/api";

interface RequestBodyPOST {
    username: string;
    password: string;
    re_password:string;
}

export async function POST(request : NextRequest){
    
    const body: RequestBodyPOST = await request.json();

    try{
        const url = generateUrl('auth/users/')

        const response_api = await fetch(url,{
            method : 'POST',
            body : JSON.stringify(body),
            headers : {
                'Content-Type' : "application/json"
            },
        })

        const data = await response_api.json()
        
        if(data.id){
            return NextResponse.json({register:true,errors:false})
        }else{
            return NextResponse.json({register:false,errors:data})
        }

    } catch(e){
        return NextResponse.json({error :"Error in request"},{status: 500})       
    }
}