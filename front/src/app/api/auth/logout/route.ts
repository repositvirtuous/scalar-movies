export const dynamic = 'force-dynamic'
import { NextRequest, NextResponse } from "next/server";


export async function POST(request : NextRequest){
    
    try{
        return NextResponse.json({logout:true},{
            headers : {
                'Set-Cookie' : 'jwt='
            }
        })
        
    } catch(e){
        return NextResponse.json({error :"Error in request"},{status: 500})       
    }
}