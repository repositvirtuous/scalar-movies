export const dynamic = 'force-dynamic'
import { NextRequest, NextResponse } from "next/server";
import { generateUrl } from "@/utils/api";

interface RequestBodyPOST {
    username: string;
    password: string;
}

export async function POST(request : NextRequest){
    
    const body: RequestBodyPOST = await request.json();

    try{
        const url = generateUrl('auth/jwt/create')

        const response_api = await fetch(url,{
            method : 'POST',
            body : JSON.stringify(body),
            headers : {
                'Content-Type' : "application/json"
            },
        })

        const data = await response_api.json()
        
        if(data.access){
            return NextResponse.json({login:true},{
                headers : {
                    'Set-Cookie' : 'jwt='+ data.access
                }
            })
        }else{
            return NextResponse.json({login:false})
        }

    } catch(e){
        console.log(e)
        return NextResponse.json({error :"Error in request"},{status: 500})       
    }
}