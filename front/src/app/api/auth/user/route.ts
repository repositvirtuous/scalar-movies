export const dynamic = 'force-dynamic'
import { NextRequest, NextResponse } from "next/server";
import { generateUrl } from "@/utils/api";

export async function GET(request : NextRequest){
    const jwt = request.cookies.get('jwt')

    if(!(jwt && jwt.value)){
        return NextResponse.json({user:false})
    }
    try{
        const url = generateUrl('auth/users/me/')

        const response_api = await fetch(url,{
            method : 'GET',
            headers : {
                'Content-Type' : "application/json",
                'Authorization' : "JWT " + jwt.value
            },
        })

        const data = await response_api.json()

        if(data.id){
            data.token = jwt.value
            return NextResponse.json({user:data})
        }
    } catch(e){
        return NextResponse.json({error :"Error in request"},{status: 500})       
    }
}