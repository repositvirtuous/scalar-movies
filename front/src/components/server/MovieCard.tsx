import { Movie } from "@/api/typescript/interfaces"
import { getImage } from '@/utils/image'


const ScoreCircle = ({ score, color } : {score:any,color:any}) => {
    return (
      <div style={{backgroundColor:'#004A8B'}} className={`mt-2 w-14 h-14 rounded-full flex items-center justify-center`}>
        <span className="text-white text-lg font-semibold">{score}</span>
      </div>
    );
  };

const MovieCard = ({movie} : {movie : Movie}) => {
    return (
        <div style={{height:'500px',width:'270px'}} className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href={"movies/" + movie.id}>
                  <img style={{height:'60%',width:'100%'}} className="rounded-t-lg" src={getImage(movie.photo)} alt="" />
              </a>
              <div className="p-5">
                <h5 style={{marginBottom:'0px'}} className="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">{movie.title}</h5>
                <span className="text-gray-400 text-sm">
                    Release date: {movie.release_date}
                </span> 
                <ScoreCircle score = {movie.average_rating} color = {"#004A8B"} />

                </div>
          </div>
    )
}

export default MovieCard