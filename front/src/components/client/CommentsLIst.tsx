'use client'

import { createRating } from "@/api/rating/rating";
import { Movie } from "@/api/typescript/interfaces"
import { useSession } from "@/contexts/AuthContext";
import Link from "next/link";
import { useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ScoreCircleMin = ({ score, color } : {score:any,color:any}) => {
    return (
      <div style={{backgroundColor:color}} className={`w-12 h-12 rounded-full flex items-center justify-center`}>
        <span className="text-white text-lg font-semibold">{score}</span>
      </div>
    );
  };

const CommentsList = ({movie} : {movie : Movie}) => {

    const [ratings,setRatings] = useState(movie.ratings)
    
    const {user,loading} = useSession()

    const submitComment = async (e : any) => {
        e.preventDefault()
        const comment = document.getElementById('comment') as HTMLInputElement
        const rating = document.getElementById('rating') as HTMLInputElement

        if(comment && rating && user){
            const create = await createRating(String(movie.id),String(user.id),rating.value,comment.value,user.token)
            if(create.rating){
                setRatings((prev_ratings) => {
                    let copy_ratings = [...prev_ratings] 
                    copy_ratings.unshift(create.rating)
                    return copy_ratings
                })
            }else{
                toast.error('Try again')
            }
        }
    }

    return (
        <div className="flex flex-col mt-8 w-full sm:w-2/3">
                <div className="flex justify-between items-center mb-6">
                    <h2 className="text-lg lg:text-2xl font-bold text-gray-900 dark:text-white">Comments</h2>
                </div>
                <form onSubmit={submitComment} className="mb-6">
                {loading
                    ? 
                    <div role="status">
                        <svg aria-hidden="true" className="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/><path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/></svg>
                        <span className="sr-only">Loading...</span>
                    </div>
                    : 
                    user 
                        ? 
                        <>
                            <div className="py-2 px-4 mb-4 bg-white rounded-lg rounded-t-lg border border-gray-200 dark:bg-gray-800 dark:border-gray-700">
                                <label htmlFor="comment" className="sr-only">Your comment</label>
                                <textarea id="comment" 
                                    className="px-0 w-full text-sm text-gray-900 border-0 focus:ring-0 focus:outline-none dark:text-white dark:placeholder-gray-400 dark:bg-gray-800"
                                    placeholder="Write a comment..." required></textarea>
                                <label htmlFor="rating" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Rating value</label>
                                <select name="rating" id="rating" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">

                                    <option value={1}>1</option>
                                    <option value={2}>2</option>
                                    <option value={3}>3</option>
                                    <option value={4}>4</option>
                                    <option value={5}>5</option>
                                    <option value={6}>6</option>
                                    <option value={7}>7</option>
                                    <option value={8}>8</option>
                                    <option value={9}>9</option>
                                    <option value={10}>10</option>
                                </select>
                            </div>
                            <button type="submit"
                                className="inline-flex items-center py-2.5 px-4 text-xs font-medium text-center text-white bg-blue-700 rounded-lg focus:ring-4 focus:ring-primary-200 dark:focus:ring-primary-900 hover:bg-primary-800">
                                Post comment
                            </button>
                        </>          
                        :           
                        <div className="space-x-4">
                            <Link className="underline text-blue-600 hover:text-blue-800 visited:text-purple-600" href="/login">Login to comment</Link>
                        </div>
                    }

                    {ratings.map((rating) => {
                        return (
                        <article key={rating.id} className="mt-2 p-6 text-base bg-white border-t border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <div className="flex justify-between items-center mb-2">
                                <div className="flex items-center">
                                    <ScoreCircleMin score = {rating.rating} color = {"#004A8B"} />
                                    <p className="ml-2 inline-flex items-center mr-3 text-sm text-gray-900 dark:text-white font-semibold">
                                        {rating.username}
                                    </p>
                                    <p className="text-sm text-gray-600 dark:text-gray-400">{rating.created_at.split('T')[0]} {rating.created_at.split('T')[1].slice(0,8)}</p>
                                </div>
                            </div>
                            <p className="text-gray-500 dark:text-gray-400">{rating.comment}</p>
                        </article>
                        )
                    })}
                </form>
            </div>
    )
}

export default CommentsList