'use client'

import { deleteMovie } from "@/api/movie/movie"
import { deleteRating } from "@/api/rating/rating"
import { useSession } from "@/contexts/AuthContext"

const DeleteButton = ({id,type} : {id:number,type:string}) => {

    const {user,loading} = useSession()

    const deleteAction = async () => {
        const response = window.confirm("Are you sure?")
        if(response && !loading && user){   
            if(type == 'movie'){
                await deleteMovie(id,user.token)
            }
            if(type == 'rating'){
                await deleteRating(id,user.token)
            }
            window.location.reload()
        }
    }   

    return (
        <>
            <button onClick={(e) => {deleteAction()}} className = "focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900" >Delete</button>
        </>
    )
}

export default DeleteButton