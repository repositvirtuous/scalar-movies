'use client'
import { Movie, User } from "@/api/typescript/interfaces"
import { getUsers } from "@/api/user/list"
import { useSession } from "@/contexts/AuthContext"
import { useEffect, useState } from "react"


const UsersTable =  ({movies} : {movies : Movie[]}) => {

    const {user,loading} = useSession()
    const [users,setUsers] = useState<User[]>([])

    useEffect(() => {
        getUsersFront()
    },[loading])

    const getUsersFront = () => {
        if(!loading){
            if(user){
                getUsers(user?.token).then((response) => {
                    setUsers(response)
                })                
            }
        }
    }

    return (
        <table className="mt-4 w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                id
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Username
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Group
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Edit
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        {users.map((user) => {
                            return (
                            <tr key = {user.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                <td scope="row" className="px-6 py-4">
                                    <a className = "underline text-blue-600 hover:text-blue-800 visited:text-purple-600" href = {"/admin/users/update/" + user.id}>{user.id}</a>
                                </td>
                                <td className="px-6 py-4">
                                    {user.username}
                                </td>
                                <td className="px-6 py-4">
                                    {user.group}
                                </td>
                                <td scope="row" className="px-6 py-4">
                                    <a className = "focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800" href = {"/admin/users/update/" + user.id}>Edit</a>
                                </td>
                            </tr>
                            )
                        })}

                    </tbody>
                </table>
    )
}   

export default UsersTable