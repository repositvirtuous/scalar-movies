'use client'
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const ToastClient = ({message,show} : {message : string,show : boolean}) => {


    useEffect(() => {
        if(show){
            toast.info(message)
        }
    },[])

    return <>
        <ToastContainer position='top-center' />
    </>
}

export default ToastClient