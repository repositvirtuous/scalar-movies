'use client'

import { getMovie, searchMoviesByTitle } from "@/api/integrations/movie_database"
import {  MovieSearch } from "@/api/typescript/interfaces"
import { useState } from "react"


const SearchMovieInDatabase = ({setFormData,setPhotoLink} : {setFormData : any,setPhotoLink : any}) => {

    const [movies,setMovies] = useState<MovieSearch[]>([])

    const searchMovies = async () => {

        const input = document.getElementById("default-search") as HTMLInputElement
        if(input){
            if(input.value.length > 3){
                const movies_result = await searchMoviesByTitle(input.value)
                if(movies_result.length > 0){
                    setMovies(movies_result)
                }else{
                    alert("No results")
                }

            }else{
                alert("Title must contains more than 3 characters")
            }

        }
    }

    const getMovieById = async  (id : string) => {

        const movie = await getMovie(id)
        if(movie){

            let genre = ''
            movie.gen.map((gen) => {
                if(genre){
                    genre+=',' + gen.genre
                }else{
                    genre+=gen.genre
                }

                const genre_input = document.getElementById("genre") as HTMLInputElement
                if(genre_input) genre_input.value = genre
                const release_date = document.getElementById("release_date") as HTMLInputElement
                if(release_date) release_date.value = movie.release
                const title_input = document.getElementById("title") as HTMLInputElement
                if(title_input) title_input.value = movie.title
                const plot_input = document.getElementById("plot") as HTMLInputElement
                if(plot_input) plot_input.value = movie.plot

                setFormData({
                    title : movie.title,
                    genre : genre,
                    plot : movie.plot,
                    release_date : movie.release
                })
                
                //TODO: Modify input file directly
                if(movie.banner){
                    setPhotoLink(movie.banner)
                }
            })

        }
    }

    return (
        <div className = "flex flex-col w-2/3 mt-4">
            <h1 className="mb-4 text-2xl font-extrabold leading-none tracking-tight text-gray-900 md:text-2xl lg:text-2xl dark:text-white">Search in MovieDatabase</h1>
            <label htmlFor="default-search" className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
            <div className="relative">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg className="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                    </svg>
                </div>
                <input type="search" id="default-search" className="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search movie by title" required/>
                <button onClick = {(e) => {searchMovies()}} type="submit" className="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
            </div>

            <div style={{height:'160px',overflowY:'scroll'}}>
                {movies.map((movie) => {
                    return (
                        <div key = {movie.imdb_id} className=" mt-2 border rounded flex flex-row w-full justify-between p-2 items-center">
                            <span>{movie.title}</span>
                            <button onClick = {(e) => {getMovieById(movie.imdb_id)}} type="button" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Fill form</button>
                        </div>
                    )
                })}

            </div>
        </div>
    )
}

export default SearchMovieInDatabase