'use client'
import { createMovie, updateMovie, updatePhotoMovie } from "@/api/movie/movie"
import { Movie } from "@/api/typescript/interfaces"
import { useSession } from "@/contexts/AuthContext"
import { useState } from "react"
import { useRouter } from "next/navigation"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SearchMovieInDatabase from "../SearchMovieInDatabase"

const MovieForm = ({type,movie} : {type : string,movie : Movie}) => {

    const router = useRouter()
    const {user} = useSession()
    const [formData,setFormData] = useState({
        title : movie.title,
        release_date : movie.release_date,
        plot : movie.plot,
        genre : movie.genre
    })
    const [photo,setPhoto] = useState(null)
    const [photoLink,setPhotoLink] = useState('')

    const onChange = (e: any) => setFormData({ ...formData, [e.target.name]: e.target.value });

    const update_movie = async (e:any) => {
        e.preventDefault()
        if(user){

            if(type == 'update'){
                const update = await updateMovie(movie.id,formData.title,formData.release_date,formData.plot,formData.genre,user?.token)
                if(update.error){
                    const message = update.error[0]
                    toast.error(message)
                }else{
                    if(photo){
                        const response_photo = await updatePhotoMovie(movie.id,photo,user.token)
                    }
                    router.push('/admin/movies?post_save=true')
                }
            }else{
                const create = await createMovie(formData.title,formData.release_date,formData.plot,formData.genre,user?.token)
                if(create.error){
                    const message = create.error[0]
                    toast.error(message)
                }else{
                    if(photo){
                        const response_photo = await updatePhotoMovie(create.movie.id,photo,user.token)
                    }
                    router.push('/admin/movies?post_create=true')
                }
            }
        }
    }

    const changePhoto = (e : any) => {
        const photo = e.target.files[0]
        if(photo){
            setPhoto(photo)
        }
    }

    return (
        <>
            <form onSubmit={update_movie} className="w-2/3">
               <ToastContainer position="top-center" />
               <h1 className="mb-4 text-2xl font-extrabold leading-none tracking-tight text-gray-900 md:text-2xl lg:text-2xl dark:text-white">{type == 'update' ? 'Update' : 'Create'} movie</h1>
                <div className="mb-6">
                    <label htmlFor="title" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Title</label>
                    <input onChange = {onChange} name = "title" defaultValue={movie.title} type="text" id="title" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="" required/>
                </div>
                <div className="mb-6">
                    <label htmlFor="release_date" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Release date</label>

                    <input name = "release_date" onChange = {onChange} defaultValue={movie.release_date} type="text" id="release_date" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required/>
                </div>
                <div className="mb-6">
                    <label htmlFor="genre" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Genre</label>
                    <input name = "genre" onChange = {onChange} defaultValue={movie.genre} type="text" id="genre" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required/>
                </div>
                <div className="mb-6">
                    <label htmlFor="plot" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Plot</label>
                    <textarea onChange = {onChange} name = "plot" defaultValue={movie.plot}  id="plot" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required/>
                </div>

                <div className = "mb-6">
                    <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white" htmlFor="photo">Upload photo</label>
                    <input onChange={(e) => {changePhoto(e)}} name="photo" className="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="photo" type="file"></input>
                    {photoLink
                    ? 
                    <div className="mt-2">Download poster: <a href={photoLink} className="underline text-blue-600 hover:text-blue-800 visited:text-purple-600" target="_blank">{photoLink}</a></div>
                    :<></>}

                </div>
            

                <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Save</button>
            

            </form>  

            <SearchMovieInDatabase setFormData={ setFormData } setPhotoLink = {setPhotoLink} /> 
        </>

    )
}

export default MovieForm