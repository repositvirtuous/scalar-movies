'use client'
import { Movie, Rating, User } from "@/api/typescript/interfaces"
import { useSession } from "@/contexts/AuthContext"
import { useEffect, useState } from "react"
import { useRouter } from "next/navigation"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getUsers } from "@/api/user/list"
import { createRating, updateRating } from "@/api/rating/rating"

const RatingForm = ({type,rating,movies} : {type : string,rating : Rating,movies : Movie[]}) => {

    const router = useRouter()
    const {user,loading} = useSession()
    const [users,setUsers] = useState<User[]>([])

    useEffect(() => {
        getUsersFront()
    },[loading])

    const getUsersFront = () => {
        if(!loading){
            if(user){
                getUsers(user?.token).then((response) => {
                    setUsers(response)
                })                
            }
        }
    }
    
    const [formData,setFormData] = useState({
        user : rating.user,
        movie : rating.movie,
        comment : rating.comment,
        rating : rating.rating
    })

    const onChange = (e: any) => setFormData({ ...formData, [e.target.name]: e.target.value });

    const update_movie = async (e:any) => {
        e.preventDefault()
        if(user){
            
            if(type == 'update'){
                const update = await updateRating(String(rating.id),String(formData.movie),String(formData.user),String(formData.rating),formData.comment,user.token)
                if(update.error){
                    const message = update.error[0]
                    toast.error(message)
                }else{
                    router.push('/admin/ratings?post_save=true')
                }
            }else{
                const create = await createRating(String(formData.movie),String(formData.user),String(formData.rating),formData.comment,user.token)
                if(create.error){
                    const message = create.error[0]
                    toast.error(message)
                }else{
                    router.push('/admin/ratings?post_create=true')
                }
            }
        }
    }

    return (
           <form onSubmit={update_movie} className="w-2/3">
               <ToastContainer position="top-center" />
               <h1 className="mb-4 text-2xl font-extrabold leading-none tracking-tight text-gray-900 md:text-2xl lg:text-2xl dark:text-white">{type == 'update' ? 'Update' : 'Create'} rating</h1>
                <div className="mb-6">
                    <label htmlFor="user" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">User</label>
                    <select value={formData.user} name="user" onChange = {onChange} id="user" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {users.map((user_select) => {
                            return (
                                <option key={user_select.id} value={user_select.id}>{user_select.id} - {user_select.username}</option>
                            )
                        })}                        
                    </select>
                </div>
                <div className="mb-6">
                    <label htmlFor="movie" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Movie</label>
                    <select value={formData.movie} name="movie" onChange = {onChange} id="movie" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {movies.map((movie) => {
                            return (
                                <option key={movie.id} value={movie.id}>{movie.id} - {movie.title}</option>
                            )
                        })}                        
                    </select>
                </div>
                <div className="mb-6">
                    <label htmlFor="rating" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Rating value</label>
                    <select value={formData.rating} name="rating" onChange = {onChange} id="rating" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option value={1}>1</option>
                        <option value={2}>2</option>
                        <option value={3}>3</option>
                        <option value={4}>4</option>
                        <option value={5}>5</option>
                        <option value={6}>6</option>
                        <option value={7}>7</option>
                        <option value={8}>8</option>
                        <option value={9}>9</option>
                        <option value={10}>10</option>
                    </select>
                </div>
                <div className="mb-6">
                    <label htmlFor="comment" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Comment</label>
                    <textarea onChange = {onChange} name = "comment" defaultValue={rating.comment}  id="comment" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required/>
                </div>
                <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Save</button>
            </form>
    )
}

export default RatingForm