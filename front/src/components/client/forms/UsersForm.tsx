'use client'
import { createMovie, updateMovie } from "@/api/movie/movie"
import { Movie, User } from "@/api/typescript/interfaces"
import { useSession } from "@/contexts/AuthContext"
import { useEffect, useState } from "react"
import { useRouter } from "next/navigation"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getUsers } from "@/api/user/list"
import { updateUser } from "@/api/user/user"

const UserForm = ({type,user_id} : {type : string,user_id : string}) => {

    const router = useRouter()
    const {user,loading,register} = useSession()
    const [userForm,setUserForm] = useState<User>({} as User)

    useEffect(() => {
        getUsersFront()
    },[loading])

    const getUsersFront = () => {
        if(!loading){
            if(user){
                getUsers(user?.token).then((response) => {
                    const user_find = response.find(u => u.id == Number(user_id))
                    if(user_find){
                        setUserForm(user_find)
                    }
                })                
            }
        }
    }

    useEffect(() => {
        if(userForm.id){
            setFormData({
                username : userForm.username,
                group : userForm.group,
                password : ''
            })
        }
    },[userForm])

    const [formData,setFormData] = useState({
        username : '',
        group : '',
        password : ''
    })

    const onChange = (e: any) => setFormData({ ...formData, [e.target.name]: e.target.value });

    const update_user = async (e:any) => {
        e.preventDefault()
        if(user){
            

            if(formData.password){
                //Validate lenght
                if(formData.password.length < 6){
                    toast.error('The password must contain more than 6 characters')
                    return
                }
            }

            if(type == 'update'){
                const update = await updateUser(Number(user_id),formData.username,formData.group,formData.password,user.token)
                if(update.error){
                    const message = update.error[0]
                    toast.error(message)
                }else{
                    router.push('/admin/users?post_save=true')
                }
            }else{
                
                if(register){
                    const create = await register(formData.username,formData.password)
                    if(!create){
                        toast.error("Error creating user")
                    }else{
                        router.push('/admin/users?post_create=true')
                    }
                }
            }
            
        }
    }

    return (
           <form onSubmit={update_user} className="w-2/3">
               <ToastContainer position="top-center" />
               <h1 className="mb-4 text-2xl font-extrabold leading-none tracking-tight text-gray-900 md:text-2xl lg:text-2xl dark:text-white">{type == 'update' ? 'Update' : 'Create'} user</h1>
                <div className="mb-6">
                    <label htmlFor="username" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
                    <input onChange = {onChange} name = "username" defaultValue={formData.username} type="text" id="title" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="" required/>
                </div>
                {type == 'update'
                ?                
                
                <div className="mb-6">
                    <label htmlFor="group" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Group</label>

                    <select value={formData.group} name="group" onChange = {onChange} id="group" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option  value="User">User</option>
                        <option value="Admin">Admin</option>
                    </select>
                </div>
                : <></>}

                <div className="mb-6">
                    <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                    <input type = "password" name = "password" onChange = {onChange} defaultValue="" id="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"/>
                </div>
                <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Save</button>
            </form>
    )
}

export default UserForm