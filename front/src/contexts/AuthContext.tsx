'use client'
import { User } from '@/api/typescript/interfaces';
import { createContext, useContext, useState, useEffect } from 'react';

interface IAuthContext {
    user : User | undefined,
    login : ((username:string,password:string) => Promise<boolean>) | undefined,
    register : ((username:string,password:string) => Promise<boolean>) | undefined,
    logout : any,
    loading : boolean
  }

const intialData: IAuthContext = {
    user: undefined,
    login : undefined,
    register : undefined,
    logout : undefined,
    loading : true
  };


/* This context abstracts the logic of a basic authentication system, with user information, login, register and logout functions   */ 
const AuthContext = createContext<IAuthContext>(intialData);

export function useSession() {
  return useContext(AuthContext);
}

export function AuthProvider({ children } : { children: React.ReactNode}) {
  const [user, setUser] = useState<User | undefined>(undefined);
  const [loading,setLoading] = useState(true)

  useEffect(() => {
    checkAuthentication();
  }, []);

  async function checkAuthentication() {
    try {
      const response = await fetch('/api/auth/user'); 
      const {user} = await response.json()
      if(user){
        setUser(user as User);
      }
    } catch (error) {
      setUser(undefined);
    }
    setLoading(false)
  }

  async function login(username : string, password : string) {
    try {

      const response = await fetch('/api/auth/login',{
          method :'POST',
          headers : {
              'Content-Type' : "application/json"
          },
          body:JSON.stringify({
            username,
            password
          })
      }); 
      const {login} = await response.json()
      await checkAuthentication()
      return login
    } catch (error) {
        return false
    }
  }

  async function register(username : string, password : string) {
    try {

        const response = await fetch('/api/auth/register',{
            method :'POST',
            headers : {
                'Content-Type' : "application/json"
            },
            body:JSON.stringify({
              username,
              password,
              re_password:password
            })
        }); 

        const {register,errors} = await response.json()
        if(errors !== false){
          let error_message = ""
          
          Object.keys(errors).map((key) => {
            errors[key].map((error:any) => {
              error_message+=error
            })
          })
          return error_message
        }

        return register
      } catch (error) {
          return false
      }
  }

  async function logout() {
    try {
        const response = await fetch('/api/auth/logout',{
            method :'POST',
            headers : {
                'Content-Type' : "application/json"
            }
        }); 
        const {logout} = await response.json()
        return logout
      } catch (error) {
          return false
      }
  }

  return (
    <AuthContext.Provider value={{ user, login, register, logout,loading }}>
      {children}
    </AuthContext.Provider>
  );
}