

export interface Rating {
    id : number;
    user : number;
    rating : number;
    comment : string;
    movie : number;
    created_at : string;
    username : string;
}

export interface Movie {
    id : number;
    title : string;
    release_date : string;
    genre : string;
    plot : string;
    ratings : Rating[];
    average_rating : number;
    photo: string;
}

export interface MovieSearch {
    imdb_id : string;
    title : string
}

export interface MovieImdb {
    imdb_id : string;
    title : string;
    release : string;
    plot : string;
    image_url : string;
    banner : string;
    gen : Genre[]
}

export interface Genre{
    id : number;
    genre : string;
}

export interface User {
    id : number;
    email : string;
    first_name : string;
    last_name : string;
    get_full_name : string;
    get_short_name : string;
    group : string;
    photo : string;
    username : string;
    token : string;
}