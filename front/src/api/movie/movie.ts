import { generateUrl, getHeaders } from "@/utils/api"
import { Movie } from "../typescript/interfaces"

const cacheConfig = {cache : 'no-store'} as RequestInit

export const getMovie = async (movie_id : string) => {

    const url = generateUrl('movies/' + movie_id)
    try{

        const response = await fetch(url,cacheConfig)
        const {movie} = await response.json()

        if (movie){
            return movie as Movie
        } else {
            return false
        }

    }catch ( error ){
        return false
    }
    
}

export const updateMovie = async (movie_id : number,title:string,release_date:string,plot:string,genre: string,token:string) => {

    const url = generateUrl('movies/' + movie_id)

    try{

        const body = {
            title,
            release_date,
            plot,
            genre
        }

        const headers = getHeaders(token)

        const response = await fetch(url,{
            method : 'PUT',
            body : JSON.stringify(body),
            headers : headers
        })

        const data = await response.json()

        return data

    }catch (error){
        return false
    }
}

export const createMovie = async (title : string, release_date:string,plot:string,genre : string,token:string) => {
   
    const url = generateUrl('movies/')

    try{
        const body = {
            title,
            release_date,
            plot,
            genre
        }

        const headers = getHeaders(token)

        const response = await fetch(url,{
            method : 'POST',
            body : JSON.stringify(body),
            headers : headers
        })
        const data = response.json()

        return data

    }catch (error){
        return false
    }
}

export const updatePhotoMovie = async (movie_id : number,photo:any,token:string) => {

    const url = generateUrl('movies/photo/')

    try{
        const formData = new FormData();
        formData.append('photo', photo);
        formData.append('movie_id',String(movie_id))

        const response = await fetch(url,{
            method : 'PUT',
            body : formData,
            headers : {
                'Authorization' : `JWT ${token}`
            }
        })

        const data = await response.json()

        return data

    }catch (error){
        return false
    }
}


export const deleteMovie = async (movie_id : number,token: string) => {

    const url = generateUrl('movies/' + movie_id)

    try{

        const headers = getHeaders(token)

        const response = await fetch(url,{
            method : 'DELETE',
            headers : headers
        })

        const data = await response.json()

        return data

    }catch (error){
        return false
    }
}