import { generateUrl } from "@/utils/api"
import { Movie } from "../typescript/interfaces"

const cacheConfig = {cache : 'no-store'} as RequestInit

export const getMovies = async (order_by : string,title:string) => {

    let url = generateUrl('movies') 

    let searchParams = new URLSearchParams()

    if(order_by){
        searchParams.set('sort_by',order_by)

        if(order_by == 'rating'){
            searchParams.set('order','desc')
        }
    }

    if(title){
        searchParams.set('title',title)
    }

    url = url + '?' + searchParams

    try{
        const response = await fetch(url,cacheConfig)
        const {movies} = await response.json()

        if (movies){
            return movies as Movie[]
        } else {
            return []
        }

    }catch ( error ){
        return []
    }
    
}