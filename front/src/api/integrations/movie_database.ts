import { generateUrl } from "@/utils/api"
import { MovieImdb, MovieSearch } from "../typescript/interfaces"

const cacheConfig = {cache : 'no-store'} as RequestInit

export const searchMoviesByTitle = async (title:string) => {

    let url = generateUrl('integrations/searchmovie/' + title) 

    try{
        const response = await fetch(url,cacheConfig)
        const {movies} = await response.json()

        if (movies){
            return movies.results as MovieSearch[]
        } else {
            return []
        }

    }catch ( error ){
        return []
    }
    
}


export const getMovie = async (imdb_id : string) => {

    let url = generateUrl('integrations/getmovie/' + imdb_id) 

    try{
        const response = await fetch(url,cacheConfig)
        const {movie} = await response.json()

        if (movie){
            return movie.results as MovieImdb
        } else {
            return false
        }

    }catch ( error ){
        return false
    }
    
}