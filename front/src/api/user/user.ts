import { generateUrl, getHeaders } from "@/utils/api"


export const updateUser = async (user_id : number,username:string,group:string,password:string,token:string) => {

    const url = generateUrl('users/' + user_id)

    try{

        const body = {
            username,
            group,
            password,
        }
        const headers = getHeaders(token)

        const response = await fetch(url,{
            method : 'PUT',
            body : JSON.stringify(body),
            headers : headers
        })

        const data = await response.json()

        return data

    }catch (error){
        return false
    }
}