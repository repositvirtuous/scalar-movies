import { generateUrl, getHeaders } from "@/utils/api"
import { User } from "../typescript/interfaces"

const cacheConfig = {cache : 'no-store'} as RequestInit

export const getUsers = async (token : string) => {

    let url = generateUrl('auth/users/') 

    try{
        const headers = getHeaders(token)


        
        const response = await fetch(url,{
            cache : 'no-store',
            headers : headers
        })
        const {results} = await response.json()

        if (results){
            return results as User[]
        } else {
            return []
        }

    }catch ( error ){
        return []
    }
    
}