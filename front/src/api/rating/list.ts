import { generateUrl } from "@/utils/api"
import { Rating } from "../typescript/interfaces"

const cacheConfig = {cache : 'no-store'} as RequestInit

export const getRatings = async () => {

    let url = generateUrl('movies/ratings/') 
    
    try{
        const response = await fetch(url,cacheConfig)
        const {ratings} = await response.json()

        if (ratings){
            return ratings as Rating[]
        } else {
            return []
        }

    }catch ( error ){
        return []
    }
    
}