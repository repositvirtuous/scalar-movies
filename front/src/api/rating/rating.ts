import { generateUrl, getHeaders } from "@/utils/api"
import { Rating } from "../typescript/interfaces"

const cacheConfig = {cache : 'no-store'} as RequestInit

export const getRating = async (rating_id : string) => {

    const url = generateUrl('movies/ratings/' + rating_id)

    try{
        const response = await fetch(url,cacheConfig)
        const {rating} = await response.json()

        if (rating){
            return rating as Rating
        } else {
            return false
        }

    }catch ( error ){
        return false
    }
    
}


export const createRating = async (movie_id:string,user_id : string,rating:string,comment : string,token:string) => {

    const url = generateUrl('movies/ratings/')

    try{

        const body = {
            movie : movie_id,
            user : user_id,
            rating : rating,
            comment : comment,
        }

        const headers = getHeaders(token)

        const response = await fetch(url,{
            method : 'POST',
            body : JSON.stringify(body),
            headers : headers
        })

        const data = await response.json()

        return data

    }catch (error){
        return false
    }
}

export const updateRating = async (rating_id : string,movie_id:string,user_id : string,rating:string,comment : string,token:string) => {

    const url = generateUrl('movies/ratings/' + rating_id)

    try{

        const body = {
            movie : movie_id,
            user : user_id,
            rating : rating,
            comment : comment,
        }
        const headers = getHeaders(token)

        const response = await fetch(url,{
            method : 'PUT',
            body : JSON.stringify(body),
            headers : headers
        })

        const data = await response.json()

        return data

    }catch (error){
        return false
    }
}


export const deleteRating = async (rating_id : number,token: string) => {

    const url = generateUrl('movies/ratings/' + rating_id)

    try{

        const headers = getHeaders(token)

        const response = await fetch(url,{
            method : 'DELETE',
            headers : headers
        })

        const data = await response.json()

        return data

    }catch (error){
        return false
    }
}